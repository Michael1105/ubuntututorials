#!/bin/bash
if [[ $(/usr/bin/id -u) -ne 0 ]]; then
echo "Can't start: Permission denied"
exit
fi
if [ ! -d /media/Data ]; then
#
sudo mkdir /media/Data;
echo "Folder has been created";
else
echo "Folder already exists";
fi;
# Choose a folder on your Data partition: 		ex. 'Documents'
# Lookup where your partition is located on the drive: 	ex. 'sda7'
if [ ! -d /media/Data/Documents ]; then
sudo mount /dev/sda7 -t ntfs-3g -o uid=1000,gid=1000,umask=002 /media/Data;
echo "Data has been mounted";
else
echo "Data was already mounted";
fi;	
#
echo "Done"
# Move 'MountData.sh' to  '/etc/init.d/': 	$ sudo mv MountData.sh /etc/init.d/
# Go to the directory and make executable: 	$ sudo chmod +x MountData.sh
# Make it run at startup: 			$ sudo update-rc.d MountData.sh defaults
